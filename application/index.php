<?php
require_once 'config.php';

try {
    $dbh = new PDO("mysql:host=mysql;dbname=". DB_NAME, DB_USER, DB_PASSWORD);
    var_dump($dbh);
} catch (PDOException $e) {
    echo "Error!: " . $e->getMessage() . "<br/>";
}