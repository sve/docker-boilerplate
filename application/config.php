<?php
/** The name of the database */
define('DB_NAME', getenv('MYSQL_ENV_MYSQL_DATABASE'));

/** MySQL database username */
define('DB_USER', getenv('MYSQL_ENV_MYSQL_USER'));

/** MySQL database password */
define('DB_PASSWORD', getenv('MYSQL_ENV_MYSQL_PASSWORD'));
