# Docker Nginx, PHP-FPM and MySQL Stack

Multicontainer application assembled and linked together with a Docker Compose.
Configuration of each docker container is available in the `docker/` directory.

Requirements
------------

 * docker 1.5.0+
 * docker-compose
 * make
 * git
 * a *nix shell

### Makefile

Customize the [Makefile](Makefile) for your needs.

Command             | Description 
------------------- | ------------
make                | Start containers in the background
make ps             | Lists containers
make log [service]  | Displays log output from service
make bash [service] | Enter service container with bash
make import         | Import SQL dump into MySQL
make restart        | Restart running containers
make stop           | Stops running containers
make rm             | Removes stopped service containers

### Environment settings

Environment           | Description             | Default
--------------------- | ----------------------- | --------------------
MYSQL_ROOT_PASSWORD   | Password for MySQL user "root"  | `secret`
MYSQL_USER            | Initial created MySQL user      | `dbuser`
MYSQL_PASSWORD        | Password for initial MySQL user | `123`
MYSQL_DATABASE        | Initial created MySQL database  | `dbname`

