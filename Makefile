CURRENT_DIRECTORY := $(shell pwd)
SERVICES := nginx fpm mysql

.SILENT:

default: cc

cc:
	@docker-compose up -d

rm:
	@docker-compose rm

stop:
	@docker-compose stop

ps:
	@docker-compose ps

log: $(SERVICES)
	@docker-compose logs $(filter-out $@, $(MAKECMDGOALS))

bash: $(SERVICES)
	@docker exec -it $$($(MAKE) -s _container $(filter-out $@, $(MAKECMDGOALS))) bash

restart:
	@docker-compose restart

import:
	@docker exec -it $$($(MAKE) -s _container mysql) sh -c 'mysql -u$$MYSQL_USER -p$$MYSQL_PASSWORD $$MYSQL_DATABASE < /tmp/schema.sql'

_container: $(SERVICES)
	@docker inspect -f '{{.Name}}' $$(docker-compose ps -q) | sed -n '/$(filter-out $@, $(MAKECMDGOALS))/s/\///p'

.PHONY: $(SERVICES) cc rm stop ps log bash import restart